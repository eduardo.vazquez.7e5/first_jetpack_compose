package com.example.temtemencyclopedia

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.expandHorizontally
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.HorizontalAlignmentLine
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.temtemencyclopedia.ui.theme.TemTemEncyclopediaTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TemTemEncyclopediaTheme {
                App()
            }
        }
    }
}

@Composable
fun App() {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 16.dp, start = 16.dp, end = 16.dp, bottom = 16.dp)
    ) {
        Image(
            painter = painterResource(id = R.drawable.blaziken),
            contentDescription = stringResource(id = R.string.blaziken),
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .size(150.dp)
                .clip(RoundedCornerShape(percent = 100))
                .border(1.dp, color = colorResource(id = R.color.orange), CircleShape)
                .padding(start = 16.dp, top = 16.dp, bottom = 24.dp, end = 16.dp)
        )
        Text(text = stringResource(id = R.string.titulo),
            fontSize = 30.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(bottom = 16.dp)
        )
        Text(text = stringResource(id = R.string.description),
            fontSize = 18.sp)
        Row(
            verticalAlignment = Alignment.Bottom,
            horizontalArrangement = Arrangement.End,
            modifier = Modifier.fillMaxSize()
        ){
            Button(onClick = { /*TODO*/ }) {
                Text(text = stringResource(id = R.string.pet))
            }
            Spacer(modifier = Modifier.padding(8.dp))
            Button(onClick = { /*TODO*/ }) {
                Text(text = stringResource(id = R.string.feed))
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(
        text = "Hello $name!",
        modifier = Modifier.padding(start = 15.dp, top = 15.dp)
    )
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    TemTemEncyclopediaTheme {
        App()
    }
}